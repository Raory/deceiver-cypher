use rand::seq::SliceRandom;
use rand::{rngs::ThreadRng, Rng};
use std::io::{self, BufRead};

const D_CHAR_LIST: &str = "βΓΔδζηΘ?θλ#μ~!¬Ξξ@ΠΣ$ΦφΨ&Ω";
const N_CHAR_LIST: &str = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";

fn rand_char(rng: &mut ThreadRng, converted: &mut Vec<char>, d_char_list: &Vec<char>) {
    for _ in 0..rng.gen_range(0..6) {
        converted.push(*d_char_list.choose(rng).unwrap());
    }
}
mod encrypt {
    use super::rand_char;
    use super::D_CHAR_LIST;
    use super::N_CHAR_LIST;
    use rand::thread_rng;

    pub fn simple(input: String) {
        let mut rng = thread_rng();
        let mut converted: Vec<char> = vec![];
        let d_char_list: Vec<char> = D_CHAR_LIST.chars().collect();
        let _n_char_list: Vec<char> = N_CHAR_LIST.chars().collect();
        println!("Proccessing input '{input}'...");
        let mut char_list: Vec<char> = input.chars().collect();
        println!("Collected input into '{:?}'...", char_list);
        char_list.reverse();
        println!("Reversed list order into '{:?}'...", char_list);
        for c in char_list {
            rand_char(&mut rng, &mut converted, &d_char_list);
            converted.push(c);
        }
        rand_char(&mut rng, &mut converted, &d_char_list);
        println!("Added random characters into {:?}", converted);
        let output: String = converted.iter().collect();
        println!("Converted list into string:\n\n\n{}\n\n", output);
    }
    //     pub fn classic(input: String) {}
    //     pub fn advanced(input: String) {}
}
/*mod decrypt {
    use super::rand_char;
    use super::D_CHAR_LIST;
    use super::N_CHAR_LIST;
    use rand::seq::SliceRandom;
    use rand::{rngs::ThreadRng, thread_rng, Rng};
        pub fn simple(input: String) {}
        pub fn classic(input: String) {}
        pub fn advanced(input: String) {}
}*/
fn main() {
    let stdin = io::stdin();
    let input = stdin.lock().lines().next().unwrap().unwrap();
    encrypt::simple(input);
}
